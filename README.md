# test

Проект создан по [ТЗ](https://docs.google.com/document/d/1lSFuIeCK2K7ZGdoddFtIUUVJeRreU-HbAviyX0eqisA/edit)

## Общая информация 

Для реализации СУБД был выбран PostgreSQL, для web-сервера Nginx.

Доступные порты на хостовой машине:
* СУБД 6543
* Web  8118

## Запуск проекта 

Необходимо выполнить следующие команды:

Клонитьровать проект 
```bash 
git clone https://gitlab.com/Dima_777/test.git
```

Перейти в директорию проекта

```bash 
cd test/
```

Запустить проект 

```bash 
docker-compose -f docker-compose.yml -p tests up -d --build
```

После запуска, данные СУБД будут расположены в __pgdata__. Отображение в Web будет производится из файла __web/index.html__. 

## Остановка проекта 

Выполнить команду:
```bash 
docker-compose -f docker-compose.yml -p tests down
```



