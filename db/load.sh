#!/usr/bin/env bash
set -x

psql -c "Create extension postgis;"

psql -c "CREATE USER test WITH password 'test';"
